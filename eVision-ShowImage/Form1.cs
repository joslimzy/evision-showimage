﻿//eVision-Show Image Sample 
//Function: 1. Load Image 
//          2. convert Bitmap to EImageC24
//          3. Show EImageC24 Image to picturebox
//Enviornment: VS 2012, eVision 2.2.2
//Caution: using eVision need to run [Visual Studio] or [compiled exe] as administrator, if not it will not able to get the grant from license dongle.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Euresys.Open_eVision_2_2;

namespace eVision_ShowImage
{
    public partial class Form1 : Form
    {
        Bitmap bitmap = null;
        BitmapData bmpData = null;
        EImageC24 EC24Image1 = new EImageC24(); //eVision的彩色圖像物件
        float ScalingRatio = 0; //Picturebox與原始影像大小的縮放比例

        public Form1()
        {
            InitializeComponent();
        }

        private void button_load_Click(object sender, EventArgs e)
        {
            //使用者選取Bitmap檔案
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //讀取檔案的Bitmap (一般這一行也可以改成從相機的影像進行讀取）
                Bitmap bitmap_source = (Bitmap)Image.FromFile(openFileDialog1.FileName);
                //當bitmap尚未創建記憶體空間時直接使用Clone複製來源影像
                if (bitmap == null)
                    bitmap = (Bitmap)bitmap_source.Clone(); //複製完整的記憶體空間
                //指派影像
                bitmap = bitmap_source; 

                //防止來源影像為空則不處理
                if (bitmap == null)
                    return;

                //Bitmap影像轉換eVision格式影像
                EC24Image1 = BitmapToEImageC24(ref bitmap);
                //顯示影像於Picturebox
                ShowImage(EC24Image1, pictureBox_image);

                //因Bitmap容易造成記憶體外洩，因此將資料從記憶體中Dispose，並且進行GC
                bitmap.Dispose();
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
            }
        }


        /// <summary>
        /// Bitmap影像轉換eVision格式影像
        /// </summary>
        /// <param name="bitmap">來源Bitmap影像</param>
        /// <returns>轉換後的EImageC24影像</returns>
        private EImageC24 BitmapToEImageC24(ref Bitmap bitmap)
        {
            EImageC24 EC24Image1 = null;
            try
            {
                EC24Image1 = new EImageC24();

                Rectangle rect = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                bmpData =
                    bitmap.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                    bitmap.PixelFormat);

                EC24Image1.SetImagePtr(bitmap.Width, bitmap.Height, bmpData.Scan0);
                bitmap.UnlockBits(bmpData);

            }
            catch (EException e)//EException為eVision的例外處理
            {
                Console.WriteLine(e.ToString());
            }
            return EC24Image1;
        }

        /// <summary>
        /// 顯示影像於Picturebox
        /// </summary>
        /// <param name="img">顯示的eVision影像</param>
        /// <param name="pb">目標Picturebox</param>
        private void ShowImage(EImageC24 img, PictureBox pb)
        {
            try
            {
                Bitmap bmp;
                bmp = new Bitmap(pb.Width, pb.Height);

                //計算Picturebox與顯示影像的比例，以便將影像縮放並且完整呈現到picturebox上。
                float PictureBoxSizeRatio = (float)pb.Width / pb.Height;
                float ImageSizeRatio = (float)img.Width / img.Height;
                if (ImageSizeRatio > PictureBoxSizeRatio)
                    ScalingRatio = (float)pb.Width / img.Width;
                else
                    ScalingRatio = (float)pb.Height / img.Height;

                //委派
                if (pb.InvokeRequired)
                {
                    pb.Invoke(new MethodInvoker(delegate() { img.Draw(Graphics.FromImage(bmp), ScalingRatio); pb.BackgroundImage = bmp; }));
                }
                else
                {
                    //先將EImageC24畫在bitmap模式的圖檔
                    img.Draw(Graphics.FromImage(bmp), ScalingRatio);
                    //再由Picturebox取得bitmap
                    pb.BackgroundImage = bmp;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
